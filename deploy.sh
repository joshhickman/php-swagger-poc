#!/binbash

echo "Starting upload progress..."

# Transpile annotations into swagger.json
./vendor/bin/swagger ./src

# Translate Swagger file into API Blueprint
swagger2blueprint swagger.json apiary.apib

#Finally, upload to Apiary
apiary publish --api-name=inmarketpetstore

echo "Upload complete"